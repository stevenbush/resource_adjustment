from vm import VM
import random, math, time

demand_sum = 0
min_migrated_num = 0
solutions_list = []
PM_Capacity = 0
result = []


def cmp(vm_a, vm_b):
    if vm_a.demand == vm_b.demand:
        return 0
    elif vm_a.demand > vm_b.demand:
        return 1
    else:
        return -1


def combi2(vm_list, m, k):
    for i in range(m, k - 1, -1):
        global STEP_NUM
        result[k - 1] = i
        if k > 1:
            combi2(vm_list, i - 1, k - 1)
        else:
            migrated_demand = 0
            for index in result:
                migrated_demand = migrated_demand + vm_list[index - 1].demand
            if demand_sum - migrated_demand <= PM_Capacity:
                current_solution = [0] * len(vm_list)
                for index in result:
                    current_solution[vm_list[index - 1].id] = 1
                global solutions_list
                solutions_list.append(current_solution)


def combi_search(vm_list, min_migrated_num):
    if min_migrated_num == 0:
        for vm in vm_list:
            vm.migrated = 0
    else:
        global result
        result = [0] * min_migrated_num

    combi2(vm_list, len(vm_list), min_migrated_num)


def generate_optimal_solutions(vm_list, input_PM_Capacity):
    global demand_sum
    demand_sum = 0
    global min_migrated_num
    min_migrated_num = 0
    global solutions_list
    solutions_list = []
    global PM_Capacity
    PM_Capacity = input_PM_Capacity

    print '[',
    for vm in vm_list:
        print '%s,' % (vm.id),
    print ']'
    print '[',
    for vm in vm_list:
        print '%s,' % (vm.demand),
    print ']'

    sorted_vm_list = vm_list[:]
    sorted_vm_list.sort(cmp)
    sorted_vm_list.reverse()

    for vm in sorted_vm_list:
        demand_sum = demand_sum + vm.demand
    print('all_item_sum=%s' % (demand_sum))
    print('PC_capacity=%s' % (PM_Capacity))

    if demand_sum > PM_Capacity:
        migrated_demand = 0
        for vm in sorted_vm_list:
            migrated_demand = migrated_demand + vm.demand
            min_migrated_num = min_migrated_num + 1
            if demand_sum - migrated_demand <= PM_Capacity:
                break

    print('min_migrated_num=%s' % (min_migrated_num))

    combi_search(vm_list, min_migrated_num)

    return solutions_list


if __name__ == '__main__':
    random.seed(10)
    PM_Capacity = 24
    vm_list = []
    for i in range(12):
        vm = VM(i, random.randint(1, 4))
        vm_list.append(vm)

    print('generate_optimal_solutions_combi:-----------------')
    start = time.clock()
    solutions_list = generate_optimal_solutions(vm_list, PM_Capacity)
    for i in range(len(solutions_list)):
        print('%s: %s' % (i, solutions_list[i]))
    end = time.clock()

    print('runtime: %s' % (end - start))
