import random, math
import networkx as nx


def cal_migrations(pre_solution, current_solution):
    migrations = 0
    for i in range(len(pre_solution)):
        migrations = migrations + abs(pre_solution[i] - current_solution[i])
    return migrations


def find_final_solution(workload_solution):
    index = 0
    solutions_to_index = []
    index_to_solutions = []
    for i in range(len(workload_solution)):
        index_list = []
        for j in range(len(workload_solution[i])):
            index_list.append(index)
            index_to_solutions.append(workload_solution[i][j])
            index = index + 1
        solutions_to_index.append(index_list)

    print('solutions_to_index: %s' % (solutions_to_index))
    print('index_to_solutions: %s' % (index_to_solutions))

    start = -1
    end = len(index_to_solutions)
    edges = []
    for i in range(len(solutions_to_index) - 1):
        for s in solutions_to_index[i]:
            for t in solutions_to_index[i + 1]:
                weight = cal_migrations(index_to_solutions[s], index_to_solutions[t])
                print('%s-%s weight: %s' % (s, t, weight))
                edges.append((s, t, weight))

    for vertex in solutions_to_index[0]:
        edges.append((start, vertex, 0))

    for vertex in solutions_to_index[len(solutions_to_index) - 1]:
        edges.append((vertex, end, 0))

    G = nx.DiGraph()
    G.add_weighted_edges_from(edges)

    shortest_path = nx.dijkstra_path(G, start, end)
    shortest_path.remove(shortest_path[0])
    shortest_path.remove(shortest_path[len(shortest_path) - 1])
    print('shortest_path: %s' % (shortest_path))
    shortest_path_length = nx.dijkstra_path_length(G, start, end)
    print('shortest_path_length: %s' % (shortest_path_length))

    print('solution_list: ')
    for val in shortest_path:
        print('%s' % (index_to_solutions[val]))


if __name__ == '__main__':
    random.seed(3)

    workload_solution = []

    for i in range(100):
        solution_list = []
        for j in range(30):
            tmp_list = []
            for k in range(12):
                tmp_list.append(random.randint(0, 1))
            solution_list.append(tmp_list)
        workload_solution.append(solution_list)

    for solution in workload_solution:
        print(solution)

    find_final_solution(workload_solution)
