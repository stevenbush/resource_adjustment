# This test file is used to test using generate_optimal_solutions_search script to generate optimal solution for a workload


import time, random, generate_optimal_solutions_search, generate_optimal_solutions_combi
from vm import VM

random.seed(2)

PM_Capacity = 24
VM_num = 12
time_span = 3

if __name__ == '__main__':

    workloads_list = []
    for i in range(time_span):
        vm_list = []
        for j in range(VM_num):
            vm = VM(j, random.randint(1, 4))
            vm_list.append(vm)
        workloads_list.append(vm_list)

    start = time.clock()
    overall_solutions_list = []
    for i in range(time_span):
        print('%s ----------------------------' % (i))
        solutions_list = generate_optimal_solutions_search.generate_optimal_solutions(workloads_list[i], PM_Capacity)
        overall_solutions_list.append(solutions_list)
    end = time.clock()

    for i in range(len(overall_solutions_list)):
        print('%s: %s ----------------------------' % (i, len(overall_solutions_list[i])))
        print('%s' % (overall_solutions_list[i]))
    end = time.clock()

    print('runtime: %s' % (end - start))
