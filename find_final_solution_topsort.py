import random, math


def RELAX(s, t, w, du_list, pre_list):
    current_val = du_list[s] + w
    if du_list[t] > current_val:
        du_list[t] = current_val
        pre_list[t] = s


def cal_migrations(pre_solution, current_solution):
    migrations = 0
    for i in range(len(pre_solution)):
        migrations = migrations + abs(pre_solution[i] - current_solution[i])
    return migrations


def print_path(pre_list, end_node):
    path_list = []
    path_list.append(end_node)
    pre_val = pre_list[end_node]
    while pre_val >= 0:
        path_list.append(pre_val)
        pre_val = pre_list[pre_val]

    path_list.reverse()
    return path_list


def find_final_solution(workload_solution):
    index = 0
    solutions_to_index = []
    index_to_solutions = []
    for i in range(len(workload_solution)):
        index_list = []
        for j in range(len(workload_solution[i])):
            index_list.append(index)
            index_to_solutions.append(workload_solution[i][j])
            index = index + 1
        solutions_to_index.append(index_list)

    print('solutions_to_index: %s' % (solutions_to_index))
    print('index_to_solutions: %s' % (index_to_solutions))

    du_list = [float('inf')] * len(index_to_solutions)
    for i in range(len(workload_solution[0])):
        du_list[i] = 0
    # print('du_list: %s' % (du_list))

    pre_list = [-1] * len(index_to_solutions)

    for i in range(len(solutions_to_index) - 1):
        for s in solutions_to_index[i]:
            for t in solutions_to_index[i + 1]:
                weight = cal_migrations(index_to_solutions[s], index_to_solutions[t])
                print('%s-%s weight: %s' % (s, t, weight))
                RELAX(s, t, weight, du_list, pre_list)

    end_node = 0
    min_path_lenght = [float('inf')]
    for val in solutions_to_index[len(solutions_to_index) - 1]:
        if du_list[val] < min_path_lenght:
            min_path_lenght = du_list[val]
            end_node = val

    shortest_path = print_path(pre_list, end_node)
    print('shortest_path: %s' % (shortest_path))
    print('shortest_path_length: %s' % (min_path_lenght))

    print('solution_list: ')
    for val in shortest_path:
        print('%s' % (index_to_solutions[val]))


if __name__ == '__main__':
    random.seed(3)

    workload_solution = []

    for i in range(100):
        solution_list = []
        for j in range(30):
            tmp_list = []
            for k in range(12):
                tmp_list.append(random.randint(0, 1))
            solution_list.append(tmp_list)
        workload_solution.append(solution_list)

    for solution in workload_solution:
        print(solution)

    find_final_solution(workload_solution)
