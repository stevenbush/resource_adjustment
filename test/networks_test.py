import networkx as nx
import matplotlib.pyplot as plt

G = nx.DiGraph()

G.add_weighted_edges_from([(1, 2, 1), (2, 'a', 1.5), ('a', 4, 1), (2, 4, 1)])

print(nx.dijkstra_path(G, 1, 4))
print(nx.dijkstra_path_length(G, 1, 4))

print(G.edges())

pos = nx.spring_layout(G)
nx.draw_networkx(G, pos)
nx.draw_networkx_edge_labels(G, pos)
plt.show()
