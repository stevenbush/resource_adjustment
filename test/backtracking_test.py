import random, math, time

random.seed(2)
vm_list = []
demand_sum = 0
PM_Capacity = 24
min_migrated_num = 0
solutions_list = []

SEARCH_NUM = 0


def tree_search(current_index, solution, migrated_num, migrated_demand, stayed_demand):
    global SEARCH_NUM
    SEARCH_NUM = SEARCH_NUM + 1

    # Already reach the optimal number of migrated vm
    if migrated_num == min_migrated_num:
        # Find a optimal solution
        if demand_sum - migrated_demand <= PM_Capacity:
            for i in range(current_index, len(solution)):
                solution[i] = 0
            current_solution = solution[:]
            solutions_list.append(current_solution)
            return
        # There is no optimal solution in current branch, stop the search.
        else:
            return

    # The current stayed demand is larger then the capacity. There is no optimal solution in current branch, stop the search.
    if stayed_demand > PM_Capacity:
        return

    if current_index <= len(vm_list) - 1 and migrated_num < min_migrated_num:
        lower_bound = math.ceil((demand_sum - migrated_demand - PM_Capacity) / vm_list[current_index])
        if migrated_num + lower_bound > min_migrated_num:
            return
        else:
            # Migrating the current VM
            solution[current_index] = 1
            current_migrated_num = migrated_num + 1
            current_migrated_demand = migrated_demand + vm_list[current_index]
            current_stayed_demand = migrated_demand
            tree_search(current_index + 1, solution, current_migrated_num, current_migrated_demand,
                        current_stayed_demand)

            # Keeping the current VM
            solution[current_index] = 0
            current_migrated_num = migrated_num
            current_migrated_demand = migrated_demand
            current_stayed_demand = migrated_demand + vm_list[current_index]
            tree_search(current_index + 1, solution, current_migrated_num, current_migrated_demand,
                        current_stayed_demand)


if __name__ == '__main__':

    vm_list = [0] * 12
    for i in range(len(vm_list)):
        vm_list[i] = random.randint(1, 4)

    vm_list.sort()
    vm_list.reverse()
    print(vm_list)
    for val in vm_list:
        demand_sum = demand_sum + val
    print('all_item_sum=%s' % (demand_sum))
    print('PC_capacity=%s' % (PM_Capacity))

    if demand_sum > PM_Capacity:
        migrated_demand = 0
        for val in vm_list:
            migrated_demand = migrated_demand + val
            min_migrated_num = min_migrated_num + 1
            if demand_sum - migrated_demand <= PM_Capacity:
                break

    print('min_migrated_num=%s' % (min_migrated_num))

    start = time.clock()
    initial_solution = [0] * len(vm_list)
    tree_search(0, initial_solution, 0, 0, 0)
    end = time.clock()

    for i in range(len(solutions_list)):
        print('%s: %s' % (i, solutions_list[i]))

    print('Search numbers: %s' % (SEARCH_NUM))
    print('runtime: %s' % (end - start))
