import random

random.seed(7)


def RELAX(s, t, w, du_list, pre_list):
    current_val = du_list[s] + w
    if du_list[t] > current_val:
        du_list[t] = current_val
        pre_list[t] = s


def print_path(pre_list):
    path_list = []
    path_list.append(10)
    pre_val = pre_list[10]
    while pre_val >= 0:
        path_list.append(pre_val)
        pre_val = pre_list[pre_val]

    path_list.reverse()
    return path_list


if __name__ == '__main__':
    levels = []
    level0 = [0]
    levels.append(level0)
    level1 = [1, 2, 3]
    levels.append(level1)
    level2 = [4, 5, 6]
    levels.append(level2)
    level3 = [7, 8, 9]
    levels.append(level3)
    level4 = [10]
    levels.append(level4)

    du_list = [float('inf')] * 11
    du_list[0] = 0
    # print du_list

    pre_list = [-1] * 11
    # print pre_list

    edges = []
    for i in range(len(levels) - 1):
        for s in levels[i]:
            for t in levels[i + 1]:
                weight = random.randint(1, 4)
                edges.append((s, t, weight))
                RELAX(s, t, weight, du_list, pre_list)

    shortest_path = print_path(pre_list)
    print('shortest_path: %s' % (shortest_path))
    print('shortest_path_length: %s' % (du_list[len(du_list) - 1]))
