import networkx as nx
import matplotlib.pyplot as plt
import random

random.seed(7)

levels = []
level0 = [0]
levels.append(level0)
level1 = [1, 2, 3]
levels.append(level1)
level2 = [4, 5, 6]
levels.append(level2)
level3 = [7, 8, 9]
levels.append(level3)
level4 = [10]
levels.append(level4)

edges = []

for i in range(len(levels) - 1):
    for s in levels[i]:
        for t in levels[i + 1]:
            weight = random.randint(1, 4)
            edges.append((s, t, weight))

G = nx.DiGraph()
G.add_weighted_edges_from(edges)

shortest_path = nx.dijkstra_path(G, 0, 10)
print('shortest_path: %s' % (shortest_path))
shortest_path_length = nx.dijkstra_path_length(G, 0, 10)
print('shortest_path_length: %s' % (shortest_path_length))

# pos = nx.circular_layout(G)
# nx.draw_networkx(G, pos)
# nx.draw_networkx_edge_labels(G, pos)
# plt.show()
