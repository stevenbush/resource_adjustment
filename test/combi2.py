import time

M = 12
N = 2
STEP_NUM = 0
RESULT_NUM = 0


def combi1(i, k):
    global STEP_NUM
    STEP_NUM = STEP_NUM + 1

    if k == N:
        global RESULT_NUM
        RESULT_NUM = RESULT_NUM + 1
        print('%s: %s' % (RESULT_NUM, result))
        return
    elif i > M:
        return
    else:
        result[k] = i
        combi1(i + 1, k + 1)
        combi1(i + 1, k)


def combi2(m, k):
    for i in range(m, k - 1, -1):
        global STEP_NUM
        STEP_NUM = STEP_NUM + 1
        result[k - 1] = i
        if k > 1:
            combi2(i - 1, k - 1)
        else:
            global RESULT_NUM
            RESULT_NUM = RESULT_NUM + 1
            print('%s: %s' % (RESULT_NUM, result))


if __name__ == '__main__':
    print('starting combi2:-----')
    result = []
    for i in range(N):
        result.append(0)
    start = time.clock()
    combi2(M, N)
    end = time.clock()
    print('step_num: %s' % (STEP_NUM))
    print('runtime: %s' % (end - start))
