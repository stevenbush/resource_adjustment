# This test file is used to test using generate_optimal_solutions_search script to generate optimal solution

import time, random, generate_optimal_solutions_search, generate_optimal_solutions_combi
from vm import VM

PM_Capacity = 24
VM_num = 12

random.seed(6)

if __name__ == '__main__':
    vm_list = []
    for i in range(VM_num):
        vm = VM(i, random.randint(1, 4))
        vm_list.append(vm)

    print('generate_optimal_solutions_search:----------------')
    start = time.clock()
    solutions_list = generate_optimal_solutions_search.generate_optimal_solutions(vm_list, PM_Capacity)
    for i in range(len(solutions_list)):
        print('%s: %s' % (i, solutions_list[i]))
    end = time.clock()

    print('runtime: %s' % (end - start))