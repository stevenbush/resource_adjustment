from vm import VM
import random, math, time

demand_sum = 0
min_migrated_num = 0
PM_Capacity = 0
solutions_list = []


def cmp(vm_a, vm_b):
    if vm_a.demand == vm_b.demand:
        return 0
    elif vm_a.demand > vm_b.demand:
        return 1
    else:
        return -1


def tree_search(current_index, vm_list, migrated_num, migrated_demand, stayed_demand):
    # Already reach the optimal number of migrated vm
    if migrated_num == min_migrated_num:
        # Find a optimal solution
        if demand_sum - migrated_demand <= PM_Capacity:
            for i in range(current_index, len(vm_list)):
                vm_list[i].migrated = 0
            current_solution = [0] * len(vm_list)
            for vm in vm_list:
                current_solution[vm.id] = vm.migrated
            global solutions_list
            solutions_list.append(current_solution)
            return
        # There is no optimal solution in current branch, stop the search.
        else:
            return

    # The current stayed demand is larger then the capacity.
    # There is no optimal solution in current branch, stop the search.
    if stayed_demand > PM_Capacity:
        return

    if current_index <= len(vm_list) - 1 and migrated_num < min_migrated_num:
        lower_bound = math.ceil((demand_sum - migrated_demand - PM_Capacity) / vm_list[current_index].demand)
        if migrated_num + lower_bound > min_migrated_num:
            return
        else:
            # Migrating the current VM
            vm_list[current_index].migrated = 1
            current_migrated_num = migrated_num + 1
            current_migrated_demand = migrated_demand + vm_list[current_index].demand
            current_stayed_demand = migrated_demand
            tree_search(current_index + 1, vm_list, current_migrated_num, current_migrated_demand,
                        current_stayed_demand)

            # Keeping the current VM
            vm_list[current_index].migrated = 0
            current_migrated_num = migrated_num
            current_migrated_demand = migrated_demand
            current_stayed_demand = migrated_demand + vm_list[current_index].demand
            tree_search(current_index + 1, vm_list, current_migrated_num, current_migrated_demand,
                        current_stayed_demand)


def generate_optimal_solutions(vm_list, input_PM_Capacity):
    global demand_sum
    demand_sum = 0
    global min_migrated_num
    min_migrated_num = 0
    global solutions_list
    solutions_list = []
    global PM_Capacity
    PM_Capacity = input_PM_Capacity

    print '[',
    for vm in vm_list:
        print '%s,' % (vm.id),
    print ']'
    print '[',
    for vm in vm_list:
        print '%s,' % (vm.demand),
    print ']'

    vm_list.sort(cmp)
    vm_list.reverse()

    for vm in vm_list:
        demand_sum = demand_sum + vm.demand
    print('all_item_sum=%s' % (demand_sum))
    print('PC_capacity=%s' % (PM_Capacity))

    if demand_sum > PM_Capacity:
        migrated_demand = 0
        for vm in vm_list:
            migrated_demand = migrated_demand + vm.demand
            min_migrated_num = min_migrated_num + 1
            if demand_sum - migrated_demand <= PM_Capacity:
                break

    print('min_migrated_num=%s' % (min_migrated_num))

    tree_search(0, vm_list, 0, 0, 0)

    return solutions_list


if __name__ == '__main__':
    random.seed(10)
    PM_Capacity = 24
    vm_list = []
    for i in range(12):
        vm = VM(i, random.randint(1, 4))
        vm_list.append(vm)

    print('generate_optimal_solutions_search:----------------')
    start = time.clock()
    solutions_list = generate_optimal_solutions(vm_list, PM_Capacity)
    for i in range(len(solutions_list)):
        print('%s: %s' % (i, solutions_list[i]))
    end = time.clock()

    print('runtime: %s' % (end - start))
